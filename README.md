Dotz - PROJETO(WebService/API)
====


!(https://login.dotz.com.br/assets/images/ball-1.png)[É sua moeda para a vida render mais.]

Escopo
------
Criar um projeto de automação com as ferramentas citadas (Cucumber + Ruby + Httparty).


Critérios de Aceite
-----------
BDD 1

1 - Acessar a API https://swapi.dev/api/films/ - ``GET``.

2 - A estrutura results apresenta diversos nomes de filmes, armazenar o valor de cada campo ``title``.

3 - Validar ``status code`` sucesso (200)

BDD 2

1 - Acessar a API http://swapi.dev/api/planets/ - ``GET``.

2 - Armazenar o valor do campo "count".

3 - Realizar uma nova requisição no mesmo endpoint, entretanto passando um valor aleatório superior ao armazenado.

4 - Validar ``status code``

5 - Validar ``mensagem apresentada``



Installation
------------

``` console
$ gem install httparty
$ gem install pry
$ gem install yaml
$ gem install report_builder
```

Variavéis globais
-----
``$response``


YAML
-----

- Utilizado como gerenciador e controle de massa de dados , integrado com o esquema de cenários.

Methods
=======

``sendRequest``
``valid_body_request``
``valid_body_request_planets``
``valid_response_code``

Obs:
=======

``Prazo Limite de Entrega: 24/03 - 23:59h (Quarta)``
