#language:pt

Funcionalidade: Consulta StarWars - Swapi



Esquema do Cenario: Consultar servico de consulta de filmes
    Dado que que realizo um request na API de consulta de filmes '<Cenario>'
    Quando valido os dados do retorno da api planets '<Cenario>'
    Entao valido o teste de acordo com os parametros estabelecidos na massa '<Cenario>'
Exemplos:
|Cenario|
|consulta_planets|

Esquema do Cenario: Consultar servico de consulta de planetas
    Dado que que realizo um request na API de consulta de filmes '<Cenario>'
    Quando valido os dados do retorno da api filmes '<Cenario>'
    Entao valido o teste de acordo com os parametros estabelecidos na massa '<Cenario>'
Exemplos:
|Cenario|
|consulta_filmes|
