require 'report_builder'

#Gera relatorio com as execuções dos cenários descritos no arquivo .features
at_exit do
  ReportBuilder.configure do |config|
    t = Time.now
    t.to_s
    config.input_path = 'data/reports'
    config.report_path = 'data/reports/Dotz - Consulta Filmes ' + t.strftime('%d_%m_%y') + ' ' + t.strftime('%H_%M_%S')
    config.report_types = [:html]
    config.report_title = 'Dotz - Teste  '
    config.additional_info = { API: 'StarWars', environment: 'Stage 5', Data_execução: t.strftime('%d/%m/%y'), Horário_Conclusão: t.strftime('%H:%M:%S') }
    config.include_images = true
  end
  ReportBuilder.build_report

  #    driver.driver_quit

end


After do |scn|
  file_name = scn.name.tr(' ', '_').downcase!
  target = "data/reports/imagens/#{file_name}.png"
  if scn.failed?
    #page.save_screenshot(target)
    embed(target, 'image/png', 'Evidência')
  elsif scn.passed?
    #page.save_screenshot(target)
    embed(target, 'image/png', 'Evidência')
  end
#  page.driver.quit
end
