require 'httparty'
require 'pry'
require 'test/unit'
require 'rubygems'


class StarWars

  #Realiza o request na URI API
  def sendRequest(massa)

binding.pry
    response = HTTParty.get(MD[massa]['path_complement'])
    p MD[massa]['path_complement']
    #Retorna o Response contendo todos os parametros do retorno do request
    return response
  end


  # valida todos as tags presentes no body da requisição
  def valid_body_request(response, massa)

    fields_request = response.to_hash





    titles = Hash.new
    for i in 0...fields_request['results'].count
      puts fields_request['results'][i]['title'].to_s
      titles[:title=>i] = fields_request['results'][i]['title']

    end



    #caso o retorno da requisicao seja OK 200 - valida os itens presentes no body


    #puts response.headers.inspect
    #puts response.code
    #puts response.body
    #puts response.message
    #p response.success?


  end

  def valid_body_request_planets(response, massa)

    # transforma response em hash chave valor
    fields_request = response.to_hash

    #captura o valor da tag count e soma + 1
    total_planets =  fields_request['count'] + Random.rand(61..100)

    #altera valor do parametro no yaml, para nova realização de novo request
    MD[massa]['path_complement'] = MD[massa]['path_complement'] + total_planets.to_s

    #realiza novo request, e captura response atual
    $response = sendRequest(massa)



  end

  #metodo responsavel por validar o status code das requisicoes
  def valid_response_code(response, massa)

    fields_request = response.to_hash

    #verifica se o response atual é igual ao esperado na massa YAML
    if response.code.to_s == MD[massa]['code_expected']
      puts "Requisicao 200 OK"


    else

      msg_details = fields_request['detail']

      if msg_details.include?(MD[massa]['msg_expected'])


        puts "Numero de planetas informado é invalido"

      else

        raise "Falha na execução!!!"
      end
    end
  end


end
