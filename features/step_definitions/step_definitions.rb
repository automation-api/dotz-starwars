=begin
Autor: Rafael Cardoso da CostaTodo
Data: 21/03/2021

=end

$response


Dado('que que realizo um request na API de consulta de filmes {string}') do |massa|
  $response = StarWars.new.sendRequest(massa)# Write code here that turns the phrase above into concrete actions
end

Quando('valido os dados do retorno da api filmes {string}') do |massa|
  StarWars.new.valid_body_request($response, massa)# Write code here that turns the phrase above into concrete actions
end

Entao('valido o teste de acordo com os parametros estabelecidos na massa {string}') do |massa|

  StarWars.new.valid_response_code($response, massa)

end


##############################

Quando('valido os dados do retorno da api planets {string}') do |massa|
  StarWars.new.valid_body_request_planets($response, massa)
end
